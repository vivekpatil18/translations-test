# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Fanalytical Inc..

"""Test helper functions."""


def ordered(obj):
    """Order a dict like object for easier testing."""
    if isinstance(obj, dict):
        return sorted((k, ordered(v)) for k, v in obj.items())
    if isinstance(obj, list):
        return sorted(ordered(x) for x in obj)
    else:
        return obj
