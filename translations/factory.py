# -*- coding: utf-8 -*-
#
# Copyright (C) 2020, 2021 Fanalytical Inc..

"""Translations general functions."""

from typing import Any, Callable, Dict, Mapping, Optional, Type

from dojson import Overdo  # type: ignore
from loguru import logger


def translation_factory(
    orgid: str,
    model: Type[Overdo],
    id_: str,
    base: Optional[Dict[str, Any]] = None,
    postprocess: Optional[Callable] = None,
) -> Callable:
    """Build translations for the organization using the DoJSON model.

    :param orgid: Organization slug.
    :param model: DoJSON Overdo model instance.
    :param base: Optional base dictionary to use for the translation result.

    :return: Function to translate a dictionary like object.
    """

    def translate(data: Mapping[str, Any]) -> Dict[str, Any]:
        """Translate ``data``."""
        res = base if base else {}
        with logger.contextualize(object_id=data.get(id_, 'unknown')):
            translated = model.do(data)
            res.update(translated)
        res = res if not postprocess else postprocess(res, data)
        if not res:
            return res

        res.setdefault('organization_id', orgid)
        return res

    return translate
