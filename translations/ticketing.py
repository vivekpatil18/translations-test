# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Fanalytical Inc..

"""Ticketing field translations."""

import arrow
from dojson import Overdo
from dojson.utils import for_each_value, ignore_value
from loguru import logger

from .factory import translation_factory
from .wrangling import create_external_id

model = Overdo()
"""Ticketing translations model."""


@model.over('external_ids', '^__TODO__$')
@for_each_value
@ignore_value
def external_ids(self, key, value):
    """External ID."""
    if not value:
        logger.error('External ID not found')
        return None
    return create_external_id('__TODO__', value)


@model.over('creation_date', '^__TODO__$')
@ignore_value
def creation_date(self, key, value):
    """Account creation date."""
    if not value:
        logger.debug('Creation date not found')

    try:
        return arrow.get(value).isoformat()
    except Exception:
        logger.error('Cannot convert {} into date for creation date.', value)
        return None


@model.over('modification_date', '^__TODO__$')
@ignore_value
def modification_date(self, key, value):
    """Account creation date."""
    if not value:
        logger.debug('Modification date not found')

    try:
        return arrow.get(value).isoformat()
    except Exception:
        logger.error(
            'Cannot convert {} into date for modification date.', value
        )
        return None


@model.over('account_id', '^__TODO__$')
@ignore_value
def account_id(self, key, value):
    """Account ID associated with the donation."""
    if not value:
        logger.error('Accounts ID not found')
        return None
    external_id = create_external_id('__TODO__', value)
    self['account_external_id'] = external_id
    return external_id


@model.over('product_id', '^__TODO__$')
@ignore_value
def product_id(self, key, value):
    """Product ID associated with the transaction."""
    if not value:
        logger.debug('Product ID not found')
        return None
    return create_external_id('__TODO__', value)


@model.over('event_id', '^__TODO__$')
@ignore_value
def event_id(self, key, value):
    """Event ID associated with the transaction."""
    if not value:
        logger.debug('Event ID not found')
        return None
    return create_external_id('__TODO__', value)


@model.over('sales_rep_id', '^__TODO__$')
@ignore_value
def sales_rep_id(self, key, value):
    """."""
    if not value:
        logger.debug('Sales Rep ID not found')
        return None
    return create_external_id('__TODO__', value)


@model.over('campaign_id', '^__TODO__$')
@ignore_value
def campaign_id(self, key, value):
    """."""
    if not value:
        logger.debug('Campaign ID not found')
        return None
    return create_external_id('__TODO__', value)


@model.over('purchase_medium', '^__TODO__$')
@ignore_value
def purchase_medium(self, key, value):
    """Purchase medium."""
    return value if value else None


@model.over('section_names', '^__TODO__$')
@for_each_value
@ignore_value
def section_names(self, key, value):
    """Extract section name and description, and row and seat names."""
    return value if value else None


@model.over('section_description', '^__TODO__$')
@for_each_value
@ignore_value
def section_description(self, key, value):
    """Section descriptions."""
    return value if value else None


@model.over('row_names', '^__TODO__$')
@for_each_value
@ignore_value
def row_names(self, key, value):
    """Row names."""
    return value if value else None


@model.over('seat_names', '^__TODO__$')
@for_each_value
@ignore_value
def seat_names(self, key, value):
    """Seat names."""
    return value if value else None


@model.over('is_group', '^__TODO__$')
@ignore_value
def is_group(self, key, value):
    """."""
    return value if value else None


@model.over('transaction_date', '^__TODO__$')
@ignore_value
def transaction_date(self, key, value):
    """Transaction date."""
    if not value:
        return
    try:
        return arrow.get(value).format('YYYY-MM-DD')
    except Exception:
        logger.error('Cannot convert {} into date for transaction date', value)
        return None


@model.over('tickets_quantity', '^__TODO__$')
@ignore_value
def tickets_quantity(self, key, value):
    """Tickets bought in the transaction."""
    if not value:
        return None

    try:
        return int(value)
    except (ValueError, TypeError) :
        logger.error('Cannot convert {} into int for number of tickets', value)
        return None


@model.over('transaction_amount', '^__TODO__$')
@ignore_value
def transaction_amount(self, key, value):
    """Total amount paid."""
    if not value:
        return None

    try:
        return float(value)
    except (ValueError, TypeError) :
        logger.error('Cannot convert {} into float for total amount', value)
        return None


translate = translation_factory(
    'test-uuid',
    model,
    id_='__TODO__',
)
"""Transform ticketing data from source to Fanalytical schema."""
