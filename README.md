## Code Test for General Data Engineers.

This is a code test for general Data Engineers at Fanalytical. Consider this an
assessment rather than a graded exam. This test helps us gauge a candidate's
level of familiarity and comfort with technologies and techniques.

### Project Description

Implement the translations to transform incoming data from an external source
into Fanalytcal's internal data model (Accounts and Ticketing). 

#### Guidance

0. We recommend that you use virtual environments for this.
   ```bash
   $ python -m venv venv # this will create a virtual environment
   $ source venv/bin/activate # activate the virtual environment
   $ (venv) make install
   ```

1. All libraries you need to implement a solution are already installed. If you'd
  like to use any other library, we would expect a reasoned explanation.
  
2. The `example_files` folder contains examples of the input we would expect
   from the source system.
   
3. `TODO` generally indicates you need to do something there ;-).
  
4. Fork this repository to your personal GitLab account and clone the fork to
   your computer. We will evaluate your solution on your personal fork.
  
5. Comment your code so that when we look at it we can understand it better.

6. There are many ways to approach and solve the problems presented in this
   exercise.
   
7. If you have any questions, just ask.
  
8. Have fun!


#### Some extra information on the tools, libraries and possible implementation

The library used for the translations,
[DoJSON](https://dojson.readthedocs.io/en/latest/usage.html),
works one object from the source at a time, one field at a time. Each decorated
function is associated with regular expression and key, the regular expression
will try to match the input, and the key will store the value on the resulting object.

If we want to create a function that transforms `{"Full_Account_ID__c":
"0010c000024PlI2AAK"}` into Fanalytcal's `external_ids` list, we'd do something
similar to this:

```python
@model.over('external_ids', '^Full_Account_ID__c$')
@for_each_value
@ignore_value
def external_ids(self, key, value):
    """External ID."""
    if not value:
        logger.error('External ID {} not found', key)
        return None
    return create_external_id('source-system-name', value)
```

And the resulting object will be:

```json
{
  "external_ids": ["source-system-name:0010c000024PlI2AAK"]
}

```

We use pytest as our testing framework of choice. The test for this function
would look like this:

```python
@pytest.mark.parametrize(
    "test_input,expected", 
    [
        ({}, {})
        (
            {'Full_Account_ID__c': '0011U00000Nfvg9QAB'},
            {'external_ids': ['salesforce:0011U00000Nfvg9QAB']}
        )
    ]
)
def test_fields(test_input, expected):
    res = accounts.translate(test_input)
    assert ordered(res) == ordered(expected)
```

There are two helper decorator that will manipulate the content of the the key:

- `@ignore_value`, will not set key on the resulting object if the function
  returns `None`.
- `@for_each_value`, will force the resulting field to be a list.


Inside each of this functions you have access to the output object at all time
via the `self` argument the function takes, so you can do things like:

```python
@model.over('type', '^(School_Alum__pc|Type)$')
@ignore_value
def type(self, key, value):
    """Account type from Account."""
    TYPES = {'alumni': 'alumni', 'customer': 'fan', 'prospect': 'prospect'}

    if key == 'School_Alum__pc':
        if 'alumni' in self.get('type', []) and value is False:
            logger.warning(
                'Found {} account type but {} is False',
                self.get('type'), key
            )
        elif 'Alumni' not in self.get('type', []) and value is True:
            current_value = self.setdefault('type', [])
            self['type'].append('alumni')

    if key == 'Type' and value is not None:
        current_value = self.setdefault('type', [])
        value = TYPES.get(value.lower(), 'other')
        if value not in current_value:
            self['type'].append(value)
```

In this case, the function doesn't return any value, but the field type is set
inside.


At the bottom of each of the translations files there is the definition of the
translation factory.  
The TODO is the key that we want to use from the source to print log messages.
This translations factory takes an optional parameter, `postprocess`, which is a
function that takes two dictionary as a parameters (raw data and translated
object) and returns a dictionary. This function is useful when there are fields
in the source that are related and/or if we want to build a field out of the
content of others, i.e. the external id.


### Solution requirements

- `make test` must succeed.
- `accounts.py` and `ticketing.py` must have at least 90% test coverage.

